//
//  ViewController.swift
//  CodeScanner
//
//  Created by Benoit Sarrazin on 2014-09-23.
//  Copyright (c) 2014 Berzerker Design. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var startButton: UIBarButtonItem!
    
    var isReading: Bool = false
    var captureSession: AVCaptureSession?
    var previewLayer: AVCaptureVideoPreviewLayer?
    var audioPlayer: AVAudioPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        captureSession = nil
        previewLayer = nil
        loadBeepSound()
    }
    
    func loadBeepSound() -> Void {
        let beepFilePath = NSBundle.mainBundle().pathForResource("beep", ofType: "mp3")
        let beepFileURL = NSURL.URLWithString(beepFilePath!)
        var error: NSError? = nil
        
        audioPlayer = AVAudioPlayer(contentsOfURL: beepFileURL!, error: &error)
        if nil != error {
            println("[\(NSStringFromClass(self.dynamicType)):\(__LINE__)] \(__FUNCTION__) | Could not play beep file: \(error?.localizedDescription)")
        } else {
            audioPlayer?.prepareToPlay()
        }
    }
    
    @IBAction func startButtonTapped(sender: UIBarButtonItem) {
        
        if isReading {
            if startReading() {
                startButton.title = "Stop"
                statusLabel.text = "Scanning..."
            }
        } else {
            startButton.title = "Start"
            stopReading()
        }
        
        isReading = !isReading
    }
    
    func startReading() -> Bool {
        var error: NSError? = nil
        let captureDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        if let input: AVCaptureDeviceInput = AVCaptureDeviceInput.deviceInputWithDevice(captureDevice, error: &error) as? AVCaptureDeviceInput {
            
            captureSession = AVCaptureSession()
            captureSession?.addInput(input)
            
            var output: AVCaptureMetadataOutput = AVCaptureMetadataOutput()
            captureSession?.addOutput(output)
            
            let dispatchQueue = dispatch_queue_create("codescanner-queue", DISPATCH_QUEUE_SERIAL)
            output.setMetadataObjectsDelegate(self, queue: dispatchQueue)
            output.metadataObjectTypes = [AVMetadataObjectTypeUPCECode,
                AVMetadataObjectTypeCode39Code,
                AVMetadataObjectTypeCode39Mod43Code,
                AVMetadataObjectTypeEAN13Code,
                AVMetadataObjectTypeEAN8Code,
                AVMetadataObjectTypeCode93Code,
                AVMetadataObjectTypeCode128Code,
                AVMetadataObjectTypePDF417Code,
                AVMetadataObjectTypeQRCode,
                AVMetadataObjectTypeAztecCode,
                AVMetadataObjectTypeInterleaved2of5Code,
                AVMetadataObjectTypeITF14Code,
                AVMetadataObjectTypeDataMatrixCode]
            
            previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            previewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
            previewLayer?.frame = previewView.layer.bounds
            previewView.layer.addSublayer(previewLayer)
            
            captureSession?.startRunning()
            return true
        } else {
            println("[\(NSStringFromClass(self.dynamicType)):\(__LINE__)] \(__FUNCTION__) | Error: Unable to initialize an input object.")
            return false
        }
    }
    
    func stopReading() -> Void {
        captureSession?.stopRunning()
        captureSession = nil
        previewLayer?.removeFromSuperlayer()
    }
    
    func captureOutput(captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [AnyObject]!, fromConnection connection: AVCaptureConnection!) {
        if nil != metadataObjects && 0 < metadataObjects.count {
            if let metadataObject: AVMetadataMachineReadableCodeObject = metadataObjects[0] as? AVMetadataMachineReadableCodeObject {
                println("[\(NSStringFromClass(self.dynamicType)):\(__LINE__)] \(__FUNCTION__) | Type: \(metadataObject.type)")
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.statusLabel.text = metadataObject.stringValue
                    self.stopReading()
                    self.startButton.title = "Start"
                    self.isReading = false
                    self.audioPlayer?.play()
                })
            }
        }
    }
}

